 1- java -jar target/floodlight.jar : 
	-lancer le controller floodlight 

 2- ./random_topology : 
	- creer une topologie aleatoire en donnant le nombre de switch de link de host ainsi que le debit max
	- genere le fichier test_topology.py qui sera utilise pour lancer mininet 

 3- sudo mn --custom test_topology.py --topo=MyTopo --mac --switch ovsk --controller=remote,ip=127.0.0.1,port=6653  --link=tc : 
	- il faut lancer la commande avec une redirection vers un fichiert t qui sera utiliser dans le programme g ( commande &> t )
	- il faut lancer la commende net
	- puis de fermer mininet avec exit
	- et le relancer sans redirection ( &> t)


/*(l'etape 4 n'est plus necessaire)
 4- curl http://127.0.0.1:8080/wm/topology/links/json :
	- commande pour avoir un fichier json qui contient les liens entre switchs
	- devra etre rediriger vers p.json ( > p.json )	
*/
	 
 5- g++ -L/path/to/json-c-0.9/lib g.cpp -o g -ljson-c : 
	- compilation du fichier g.cpp

 6- ./g 1 2 : 
	- lancer le programme g avec le numeero du host source et du host destination
	- ce qui nous donne le chemin le plus court entre les deux hosts
	- genere le fichier SCRIPT.py qui contient les flows a implementer dans les differents switchs du chemins

 7- python SCRIPT.py : 
	- lance le scipt qui implementera les differents switchs
