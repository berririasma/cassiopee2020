// -L/path/to/json-c-0.9/lib g.cpp -o g -ljson-c

// CPP code for printing shortest path between 
// two vertices of unweighted graph 
#include <bits/stdc++.h>

#include <json-c/json.h>

#include <stdio.h>

#include <stdlib.h>

#include <math.h>

#include <string.h>

#include <time.h> 
using namespace std; 
  
// utility function to form edge between two vertices 
// source and dest 
void add_edge(vector<int> adj[], int src, int dest) 
{ 
    adj[src].push_back(dest); 
    adj[dest].push_back(src); 
} 
  
// a modified version of BFS that stores predecessor 
// of each vertex in array p 
// and its distance from source in array d 
bool BFS(vector<int> adj[], int src, int dest, int v, 
                            int pred[], int dist[]) 
{ 
    // a queue to maintain queue of vertices whose 
    // adjacency list is to be scanned as per normal 
    // DFS algorithm 
    list<int> queue; 
  
    // boolean array visited[] which stores the 
    // information whether ith vertex is reached 
    // at least once in the Breadth first search 
    bool visited[v]; 
  
    // initially all vertices are unvisited 
    // so v[i] for all i is false 
    // and as no path is yet constructed 
    // dist[i] for all i set to infinity 
    for (int i = 0; i < v; i++) { 
        visited[i] = false; 
        dist[i] = INT_MAX; 
        pred[i] = -1; 
    } 
  
    // now source is first to be visited and 
    // distance from source to itself should be 0 
    visited[src] = true; 
    dist[src] = 0; 
    queue.push_back(src); 
  
    // standard BFS algorithm 
    while (!queue.empty()) { 
        int u = queue.front(); 
        queue.pop_front(); 
        for (int i = 0; i < adj[u].size(); i++) { 
            if (visited[adj[u][i]] == false) { 
                visited[adj[u][i]] = true; 
                dist[adj[u][i]] = dist[u] + 1; 
                pred[adj[u][i]] = u; 
                queue.push_back(adj[u][i]); 
  
                // We stop BFS when we find 
                // destination. 
                if (adj[u][i] == dest) 
                   return true; 
            } 
        } 
    } 
  
    return false; 
} 
  
// utility function to print the shortest distance  
// between source vertex and destination vertex 
void printShortestDistance(vector<int> adj[], int s,  
                                    int dest, int v, int nhost) 
{ 
    // predecessor[i] array stores predecessor of 
    // i and distance array stores distance of i 
    // from s 
    int pred[v], dist[v]; 
  
    if (BFS(adj, s, dest, v, pred, dist) == false) 
    { 
        cout << "Given source and destination"
             << " are not connected"; 
        return; 
    } 
  
    // vector path stores the shortest path 
    vector<int> path; 
    int crawl = dest; 
    path.push_back(crawl); 
    while (pred[crawl] != -1) { 
        path.push_back(pred[crawl]); 
        crawl = pred[crawl]; 
    } 
  
    // distance from source is in distance array 
    cout << "Shortest path length is : "
        << dist[dest]; 
  
    // printing path from source to destination 
    cout << "\nPath is::\n"; 
    for (int i = path.size() - 1; i >= 0; i--) {
        if(path[i] > nhost) cout << "s" << path[i] - nhost << " "; 
	else  cout <<"h" << path[i] << " "; 
	}
	cout << endl;
	FILE *fptr,*fp;

	char str[100],str2[100],st[1024];
	char tab,tab1;
	int ss,dd;
	//char s[100][500];
	fptr=fopen("SCRIPT.py", "w");

	fp=fopen("p.json", "r");
	//struct json_object* parsed_json;
	//struct json_object* src_switch;
	printf("%s",str);
	fprintf(fptr, "import urllib2\n");
	fseek(fp,0, SEEK_END);	
	int d = ftell(fp)/152;	
	printf("%d",d);
	int k , ll =0, num=0;
	sprintf(st, "url = 'http://localhost:8080/wm/staticflowpusher/json'");
	fprintf(fptr, "%s\n", st);
	FILE *f=fopen("t", "r");
	size_t len =0;
	char * tep;
	    printf("  %ld  \n",ftell(f));
	do{
		getline(&tep,&len,f);
	}while(strcmp(tep,"*** Starting CLI:") != 10);
	int debut = ftell(f);	
	char* cmp;
	
	for (int i= path.size() - 2; i >= 1; i--){
		cmp[0]='s';
		cmp[2]='\0';
		cmp[1]='0'+path[i]-nhost;	
		do{
			getline(&tep,&len,f);
			printf("%s",tep);
			str[0]=tep[0];//+tep[1]+'\0';
			str[1]=tep[1];
			str[2]='\0';
		printf(" %s   %s  %d  %d  XX\n",str,cmp,path[i]-nhost,strcmp(str,"s"+path[i]-nhost));	
			getchar();
		}while(strcmp(str,cmp) != 0);
		printf(" %d \n",strcmp(str,"s"+path[i]-nhost));
		fseek(f, debut, SEEK_SET);
		int k=0,l=0,mm=0,nn=0,pl=0;
		char komp,konp,komp1='0'+path[i-1]-nhost,komp2=path[i+1]-nhost;
		char src;
		do{
			ss = str[1]-'0';
		printf(" %c \n",tep[17+k*16]);
printf(" %c  %d  %d  %d %d\n",tep[16+k*16],dest,i,tep[17+k*16] -'0',(tep[17+k*16] -'0'== d));
		if(tep[16+k*16]=='h' && l==0 && i == path.size() - 2){
			if(tep[17+k*16] -'0'== s){
			tab1=tep[14+k*16]-'0';	
				printf("*/ %c /*",tep[14+k*16]);
			mm=1;		
getchar();	
			}
		}
else if(tep[16+k*16]=='h'&& i == 1){
getchar();
		if(tep[17+k*16] -'0'== dest){
			tab=tep[14+k*16]-'0';	
			printf("*/ %c /*",tep[14+k*16]);
			mm=1;		
getchar();	
			int toss=0;
			do{
if(tep[16+toss*16]=='s'){
	getchar();
	konp = tep[17+toss*16]-'0';
	tab1 = tep[14+toss*16]-'0';
	komp=komp1;
	}
toss++;
printf(" %d  %d\n",komp2,konp);
getchar();
	}while(komp2!=konp);
nn=1;
	
		printf(" %d  %d  \n",tab,tab1);		}
}
		else if(tep[16+k*16]=='s' && i!=1){
getchar();	
			komp = tep[17+k*16];
			printf(" %c  %c  %c  %d  \n",tep[14+k*16],tep[22+k*16],komp,path[i-1]-nhost);
			tab = tep[14+k*16]-'0';
			//tab1 = tep[22+k*16]-'0';	
			ss = str[1]-'0';
			dd = komp-'0';
			printf(" %d  %d\n",ss,dd);
			nn=1;
			
		}
if( i != path.size() - 2 && i!=1 && mm==0) {
	do{
if(tep[16+pl*16]=='s'){
	getchar();
	konp = tep[17+pl*16]-'0';
	tab1 = tep[14+pl*16]-'0';
}
pl++;
printf(" %d  %d\n",komp2,konp);
	}while(komp2!=konp);
mm=1;
}
l=nn+mm;
		k++;
		}while(l!=2 || komp!=komp1);
		printf(" %d  %d  \n",s,dest);
		printf(" %d  %d  \n",tab,tab1);

	sprintf(st, "data%d = '{\"switch\":\"00:00:00:00:00:00:00:0%d\",\"name\":\"flow-%d\",\"priority\":\"32768\",\"in_port\":\"%d\",\"active\":\"true\", \"eth_type\":\"0x0800\",\"eth_src\":\"00:00:00:00:00:0%d\",\"eth_dst\":\"00:00:00:00:00:0%d\",\"ipv4_src\":\"10.0.0.%d\",\"ipv4_dst\":\"10.0.0.%d\", \"actions\":\"output=%d\"}'",num,ss,ll++,tab1,s,dest,s,dest,tab);
	
	fprintf(fptr, "%s\n", st);
	sprintf(st,"req%d = urllib2.Request(url, data%d, {'Content-Type': 'application/json'})",num,num);

	fprintf(fptr, "%s\n", st);

	sprintf(st,"f = urllib2.urlopen(req%d)",num);
	fprintf(fptr, "%s\n", st);
	num++;

	sprintf(st, "data%d = '{\"switch\":\"00:00:00:00:00:00:00:0%d\",\"name\":\"flow-%d\",\"priority\":\"32768\",\"in_port\":\"%d\",\"active\":\"true\", \"eth_type\":\"0x0800\",\"eth_src\":\"00:00:00:00:00:0%d\",\"eth_dst\":\"00:00:00:00:00:0%d\",\"ipv4_src\":\"10.0.0.%d\",\"ipv4_dst\":\"10.0.0.%d\", \"actions\":\"output=%d\"}'",num,ss,ll++,tab,dest,s,dest,s,tab1);
		getchar();	
fprintf(fptr, "%s\n", st);

sprintf(st,"req%d = urllib2.Request(url, data%d, {'Content-Type': 'application/json'})",num,num);

	fprintf(fptr, "%s\n", st);

	sprintf(st,"f = urllib2.urlopen(req%d)",num++);

	fprintf(fptr, "%s\n", st);

		getchar();	

}
	/*for (int i= path.size() - 2; i > 1; i--){
	int j=0;
	do{	fseek(fp,152*(j)+ 38, SEEK_SET);
		fgets(str, sizeof(char)*2, fp);
		printf("    %d  %d X  ",path[i]-nhost,*str-'0');
		fseek(fp,152*(j) +90, SEEK_SET);
		fgets(str2, sizeof(char)*2, fp);
		printf("    %d  %d X  ",path[i-1]-nhost,*str2-'0');
                j++;
		printf("/ %d /",*str - '0' != path[i]-nhost);


		printf("%d %d",d,j);
		getchar();
	}while( ( (*str - '0' != path[i]-nhost) || (*str2 - '0' != path[i-1]-nhost)) &&  ( (*str- '0' != path[i-1]-nhost)  ||  (*str2 - '0' != path[i]-nhost) ));

*/

/*	printf("\n%d %d\n",d,j);
	fseek(fp,152*(j-1)+ 38, SEEK_SET);
	fgets(str, sizeof(char)*2, fp);
	tab[0]=*str - '0';
	fseek(fp,152*(j-1)+ 52, SEEK_SET);
	fgets(str, sizeof(char)*2, fp);
	tab[1]=*str - '0';
	fseek(fp,152*(j-1) +90, SEEK_SET);
	fgets(str, sizeof(char)*2, fp);
	tab[2]=*str - '0';
	fseek(fp,152*(j-1)+ 104, SEEK_SET);
	fgets(str, sizeof(char)*2, fp);
	tab[3]=*str - '0';
	
*/
	/*for(int te=0;te<4;te++) printf("%d\n", tab[te]);
	sprintf(st, "data%d = '{\"switch\":\"00:00:00:00:00:00:00:0%d\",\"name\":\"flow-%d\",\"priority\":\"32768\",\"in_port\":\"%d\",\"active\":\"true\", \"eth_type\":\"0x0800\",\"eth_src\":\"00:00:00:00:00:0%d\",\"eth_dst\":\"00:00:00:00:00:0%d\",\"ipv4_src\":\"10.0.0.%d\",\"ipv4_dst\":\"10.0.0.%d\", \"actions\":\"output=%d\"}'",num,tab[0],ll++,tab[1],path[0],path[path.size()-1],path[0],path[path.size()-1],tab[2]);

	fprintf(fptr, "%s\n", st);
	sprintf(st,"req%d = urllib2.Request(url, data%d, {'Content-Type': 'application/json'})",num,num);
	fprintf(fptr, "%s\n", st);
	sprintf(st,"f = urllib2.urlopen(req%d)",num);
	fprintf(fptr, "%s\n", st);
	num++;
	sprintf(st, "data%d = '{\"switch\":\"00:00:00:00:00:00:00:0%d\",\"name\":\"flow-%d\",\"priority\":\"32768\",\"in_port\":\"%d\",\"active\":\"true\", \"eth_type\":\"0x0800\",\"eth_src\":\"00:00:00:00:00:0%d\",\"eth_dst\":\"00:00:00:00:00:0%d\",\"ipv4_src\":\"10.0.0.%d\",\"ipv4_dst\":\"10.0.0.%d\", \"actions\":\"output=%d\"}'",num,tab[2],ll++,tab[3],path[path.size()-1],path[0],path[path.size()-1],path[0],tab[1]);
fprintf(fptr, "%s\n", st);
sprintf(st,"req%d = urllib2.Request(url, data%d, {'Content-Type': 'application/json'})",num,num);
	fprintf(fptr, "%s\n", st);
	sprintf(st,"f = urllib2.urlopen(req%d)",num++);
	fprintf(fptr, "%s\n", st);
//http://localhost:8080/wm/staticflowpusher/json);
	} */
sprintf(st, "for x in f:\n    print(x)\nf.close()");
fprintf(fptr, "%s\n", st);

}
  
	
// Driver program to test above functions 
int main(int argc, char**argv) 
{ 
	FILE *f;
	char str[100];
	f=fopen("t", "r");
	int i=0;
	char hosts[100][100];
	char links[100][100];
	char switches[100][100];

	int nswit,nhost,nlink;

	while( i < 9){
		fscanf(f,"%s",str);
		i++;
	}
	i=0;
	fscanf(f,"%s",str);
	while(strcmp(str,"***")!=0){
		strcpy(hosts[i],str);
		i++;
		fscanf(f,"%s",str);
	}
	/*for(int j=0; j<i; j++){
		printf("%s\n",hosts[j]);
	}*/
	nhost=i;
	i=0;
	while( i < 2){
		fscanf(f,"%s",str);
		i++;
	}
	i=0;
	fscanf(f,"%s",str);
	while(strcmp(str,"***")!=0){
		strcpy(switches[i],str);
		i++;
		fscanf(f,"%s",str);
	}
	/*for(int j=0; j<i; j++){

		printf("%s\n",switches[j]);

	}*/
	nswit=i;
	i=0;
	while( i < 2){
		fscanf(f,"%s",str);
		i++;
	}
	i=0;
	fscanf(f,"%s",str);
	fscanf(f,"%s",str);
	//fgets(str, sizeof(char)*20, f);
	while(strcmp(str,"Configuring")!=0){
		fscanf(f,"%s",str);
		strcat(links[i],str);
		fscanf(f,"%s",str);
		strcat(links[i],str);
		i++;
		fscanf(f,"%s",str);
		fscanf(f,"%s",str);	
		//fgets(str, sizeof(char)*20, f);
	}
	/*for(int j=0; j<i; j++){

		printf("%s\n",links[j]);
	}*/
	nlink=i;
	int src,dst;
    // no. of vertices 
    int v = nhost+nswit+1;
    // array of vectors is used to store the graph 
    // in the form of an adjacency list 
    vector<int> adj[v]; 
	for(int j=0; j<nlink; j++){
		if(links[j][1] == 's') src=(int)links[j][2]-'0'+nhost;
		else src=(int)links[j][2]-'0';
		if(links[j][4] == 's') dst=(int)links[j][5]-'0'+nhost;
		else dst=(int)links[j][5]-'0';
		add_edge(adj, src, dst); 
	}
  
    // Creating graph given in the above diagram. 
    // add_edge function takes adjacency list, source  
    // and destination vertex as argument and forms 
    // an edge between them.
    int source = *argv[1]-'0', dest = *argv[2]-'0'; 
    printShortestDistance(adj, source, dest, v, nhost); 
    return 0; 
//  curl http://127.0.0.1:8080/wm/topology/links/json
//  curl http://127.0.0.1:8080/wm/core/controller/switches/json

} 


