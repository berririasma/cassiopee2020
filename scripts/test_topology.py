#!/usr/bin/python
from mininet.topo import Topo


class MyTopo( Topo ):
	def __init__( self ):
		Topo.__init__( self )
		s1 = self.addSwitch('s1')
		s2 = self.addSwitch('s2')
		s3 = self.addSwitch('s3')
		s4 = self.addSwitch('s4')
		s5 = self.addSwitch('s5')
		s6 = self.addSwitch('s6')
		s7 = self.addSwitch('s7')
		s8 = self.addSwitch('s8')
		h1 = self.addHost('h1')
		self.addLink(s6, h1, bw=20)
		h2 = self.addHost('h2')
		self.addLink(s4, h2, bw=20)
		h3 = self.addHost('h3')
		self.addLink(s2, h3, bw=20)
		h4 = self.addHost('h4')
		self.addLink(s7, h4, bw=20)
		self.addLink(s1, s3, bw=7)
		self.addLink(s1, s5, bw=10)
		self.addLink(s1, s6, bw=10)
		self.addLink(s2, s8, bw=7)
		self.addLink(s3, s4, bw=13)
		self.addLink(s4, s8, bw=5)
		self.addLink(s5, s7, bw=20)
		self.addLink(s7, s8, bw=3)
topos = { 'MyTopo': ( lambda: MyTopo() ) }
